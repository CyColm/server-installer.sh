# server-installer.sh

script to install server on Debian
Made by Dylan Adam aka elessar41 on github

This script runs a switch that allows to install :
1) LAMP
2) Git
3) Composer
4) MySQL user
5) PHP extensions
6) Rewrite mod Apache
7) Set the Vhost server
8) FTP server

Of course, not necessary to use all of cases.
If needed, you can change differents cases.
This script was for my training, to learn how to install a website on a raspberry.