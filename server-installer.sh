#! /bin/bash
# a lamp server installer (apache + mysql + php)
# author: Dylan Adam

while true; do

    echo '██╗      █████╗ ███╗   ███╗██████╗     ██╗███╗   ██╗███████╗████████╗ █████╗ ██╗     ██╗     ███████╗██████╗'
    echo '██║     ██╔══██╗████╗ ████║██╔══██╗    ██║████╗  ██║██╔════╝╚══██╔══╝██╔══██╗██║     ██║     ██╔════╝██╔══██╗'
    echo '██║     ███████║██╔████╔██║██████╔╝    ██║██╔██╗ ██║███████╗   ██║   ███████║██║     ██║     █████╗  ██████╔╝'
    echo '██║     ██╔══██║██║╚██╔╝██║██╔═══╝     ██║██║╚██╗██║╚════██║   ██║   ██╔══██║██║     ██║     ██╔══╝  ██╔══██╗'
    echo '███████╗██║  ██║██║ ╚═╝ ██║██║         ██║██║ ╚████║███████║   ██║   ██║  ██║███████╗███████╗███████╗██║  ██║'
    echo '╚══════╝╚═╝  ╚═╝╚═╝     ╚═╝╚═╝         ╚═╝╚═╝  ╚═══╝╚══════╝   ╚═╝   ╚═╝  ╚═╝╚══════╝╚══════╝╚══════╝╚═╝  ╚═╝'
    echo ''
    echo '@Author: Dylan Adam'
    echo ''
    echo 'Choose an option :'
    echo '  [1] Install Lamp'
    echo '  [2] Install Git'
    echo '  [3] Install Composer'
    echo '  [4] Create Mysql User'
    echo '  [5] Install all PHP Extensions'
    echo '  [6] Enable rewrite mod Apache'
    echo '  [7] Configure Vhost server'
    echo '  [8] Install FTP server'
    echo '  [9] Exit or (Ctrl + C)'
    echo ''
    read -p '[~>] Enter your choice : ' choice

    case $choice in
        1)
            echo '[*] Install Lamp...'
            sudo apt install apache2 mysql-server php libapache2-mod-php -y
            ;;
        2)
            echo '[*] Install Git...'
            sudo apt install git -y
            ;;
        3)
            echo '[*] Install Composer...'
            sudo apt install composer -y
            ;;
        4)
            read -p '[~>] Enter name of user : ' name
            read -p '[~>] Enter password for user : ' password
            mysql -u root --execute="CREATE USER '$name'@localhost IDENTIFIED BY '$password';"
            mysql -u root --execute="GRANT ALL PRIVILEGES ON *.* TO '$name'@'localhost' WITH GRANT OPTION;"
            ;;
        5)
            echo '[*] Install required extensions to php...'
            sudo apt install php-cli php-common php-curl php-gd php-json php-mbstring php-mysql php-opcache php-readline php-xml php-fpm php-mcrypt php-apcu php-bcmath php-intl php-zip php-soap php-sqlite3 -y
            ;;
        6)
            echo '[*] Enable apache rewrite engine...'
            a2enmod rewrite
            sudo systemctl restart apache2
            echo '[*] Apache mod enable success !'
            ;;
        7)
            read -p "[~>] Enter app name : " app_name
            sudo nano /etc/apache2/sites-available/$app_name.conf
            systemctl restart apache2
            ;;
        8)
            echo "[*] Install proftpd..."
            sudo apt install proftpd -y
            echo "[*] Create user to FTP connexion"
            read -p "[~>] Enter user name : " user_ftp
            sudo adduser $user_ftp
            echo "[*] You can use FileZilla with $user_ftp and password to connect ftp"
            ;;
        9)
            echo "Bye =)"
            break
            ;;
    esac
done
